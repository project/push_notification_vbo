<?php

namespace Drupal\push_notification_vbo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements Firebase API configuration.
 *
 * Creates the administrative form with settings used to
 * connect with Firebase Cloud Messassing API.
 *
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'push_notification_vbo.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['push_notification_vbo.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('push_notification_vbo.settings');

    $form['push_notification_vbo'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Push Notification VBO'),
      '#open' => TRUE,
    ];

    $form['push_notification_vbo']['payload_keys'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Payload keys'),
      '#description' => $this->t('Payload keys (ex: nid, type, screen), one per line'),
      '#default_value' => $config->get('payload_keys'),
      '#required' => FALSE,
    ];

    $form['push_notification_vbo']['payload_values'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Payload values'),
      '#description' => $this->t('Add field types (textfield, select etc...)
       and with concatenate sign (|) the values, comma separated
       (ex: news, course) one per line'),
      '#default_value' => $config->get('payload_values'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('push_notification_vbo.settings');
    $config
      ->set('payload_keys', $form_state->getValue('payload_keys'))
      ->set('payload_values', $form_state->getValue('payload_values'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
