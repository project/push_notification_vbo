<?php

namespace Drupal\push_notification_vbo\Plugin\Action;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\firebase\Service\FirebaseMessageService;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsPreconfigurationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * A sending Push Notification action.
 *
 * @Action(
 *   id = "send_push_action",
 *   label = @Translation("Send Push Notification"),
 *   type = "",
 *   confirm = TRUE,
 * )
 */
class SendPushAction extends ViewsBulkOperationsActionBase implements ViewsBulkOperationsPreconfigurationInterface, ContainerFactoryPluginInterface {
  protected $pushService;
  protected $entityTypeManager;

  /**
   * The tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FirebaseMessageService $pushService,
    EntityTypeManagerInterface $entityTypeManager,
    PrivateTempStoreFactory $tempStoreFactory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $pushService, $entityTypeManager, $tempStoreFactory);
    $this->push_service = $pushService;
    $this->entity_type_manager = $entityTypeManager;
    $this->tempStoreFactory = $tempStoreFactory;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('firebase.message'),
      $container->get('entity_type.manager'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Checks object access.
   *
   * @param mixed $object
   *   The object to execute the action on.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The user for which to check access, or NULL to check access
   *   for the current user. Defaults to NULL.
   * @param bool $return_as_object
   *   (optional) Defaults to FALSE.
   *
   * @return bool|\Drupal\Core\Access\AccessResultInterface
   *   The access result. Returns a boolean if $return_as_object is FALSE (this
   *   is the default) and otherwise an AccessResultInterface object.
   *   When a boolean is returned, the result of AccessInterface::isAllowed() is
   *   returned, i.e. TRUE means access is explicitly allowed, FALSE means
   *   access is either explicitly forbidden or "no opinion".
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object instanceof EntityInterface) {
      return $object->access('update', $account, $return_as_object);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreConfigurationForm(array $form, array $values, FormStateInterface $form_state) {

    $display_id = $form_state->get('display_id');
    /** @var \Drupal\views_ui\ViewUI $view_ui */
    $view_ui = $form_state->get('view');
    $display_list = $view_ui->get('display');

    if (isset($display_list[$display_id]['display_options']['fields'])) {
      $fields = $display_list[$display_id]['display_options']['fields'];
    }
    else {
      $fields = $display_list['default']['display_options']['fields'];
    }

    $fields_options = [];
    foreach ($fields as $key => $field) {
      $fields_options[$key] = $field['label'];
    }

    $form['device_token_field_name'] = [
      '#type' => 'select',
      '#title' => t("Field used for storing a device's registration token"),
      '#description' => t("Choose a field from the current view that will be used as device's registration token."),
      '#options' => $fields_options,
      '#default_value' => isset($values['device_token_field_name']) ? $values['device_token_field_name'] : '',
      '#required' => TRUE,
    ];

    $form['maximum_payload_number'] = [
      '#type' => 'number',
      '#title' => t("Maximum number of key-value pairs to show on the VBO form."),
      '#description' => t("This number of key-value pair fields will turn up when you send push notification. Deafult is 10."),
      '#default_value' => isset($values['maximum_payload_number']) ? $values['maximum_payload_number'] : 10,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Configuration form builder.
   *
   * If this method has implementation, the action is
   * considered to be configurable.
   *
   * @param array $form
   *   Form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The configuration form.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Get views display's relationship info to generate token types.
    /** @var \Drupal\views\ViewEntityInterface $view */
    $view = $this->entity_type_manager->getStorage('view')->load($this->context['view_id']);
    $view_executable = $view->getExecutable();
    $view_executable->setDisplay($this->context['display_id']);
    $relationships = $view_executable->display_handler->getHandlers('relationship');

    // Get base entity type of the view.
    $entity_type = $view_executable->getBaseEntityType()->id();
    $relationship_token_types[$entity_type] = $entity_type;

    foreach ($relationships as $relationship) {
      if (array_key_exists('entity type', $relationship->definition)) {
        $entity_type = $relationship->definition['entity type'];
        $relationship_token_types[$entity_type] = $entity_type;
      }
    }

    $config = \Drupal::service('config.factory')->getEditable('push_notification_vbo.settings');
    $remember_push_notification = $config->get('remember_push_notification');
    $push_notification_title = $config->get('push_notification_title');
    $push_notification_body = $config->get('push_notification_body');
    $payload_keys = $config->get('payload_keys');
    $payload_values = $config->get('payload_values');

    $maximum_payload_number = $this->context['preconfiguration']['maximum_payload_number'] * 1;

    $payload = $config->get('payload');

    $form['#tree'] = TRUE;
    $i = 0;
    $payload_field = $form_state->getValue('num_payload');
    if (empty($payload_field)) {
      $form_state->setValue('num_payload', $maximum_payload_number);
      $payload_field = $form_state->getValue('num_payload');
    }
    $form['#title'] = $this->t('Send Push Notification');

    $form['push_notification_title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'textfield',
      '#default_value' => ($remember_push_notification == 1) ? $push_notification_title : NULL,
    ];
    $form['push_notification_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#description' => $this->t('Enter message'),
      '#default_value' => ($remember_push_notification == 1) ? $push_notification_body : NULL,
      '#rows' => 3,
    ];
    $form['payload_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Payload'),
      '#prefix' => '<div id="payload-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#attributes' => ['class' => ['container-inline']],
      '#open' => FALSE,
      '#description' => $this->t('<p>With FCM, you can send two types of messages to clients:</p><br /><ul><li>Notification messages, sometimes thought of as "display messages." These are handled by the FCM SDK automatically.</li><li>Data messages, which are handled by the client app.</li><p>Notification messages contain a predefined set of user-visible keys. Data messages, by contrast, contain only your user-defined custom key-value pairs. Notification messages can contain an optional data payload. Maximum payload for both message types is 4KB, except when sending messages from the Firebase console, which enforces a 1024 character limit.</p>'),
    ];
    if ($payload_keys && $payload_values) {
      $pkeys = explode(PHP_EOL, $payload_keys);
      $pvalues = explode(PHP_EOL, $payload_values);
      $payload_field = count($pkeys);
      $pvalues_array = [];
      if (is_array($pvalues) && !empty($pvalues)) {
        foreach ($pvalues as $pvalue) {
          $parray = explode('|', $pvalue);
          $pvalues_array[][$parray[0]] = explode(',', $parray[1]);
        }
      }
    }
    if (is_array($pvalues_array) && !empty($pvalues_array)) {
      for ($i = 0; $i < $payload_field; $i++) {
        $type = array_keys($pvalues_array[$i])[0];
        $form['payload_fieldset']['payload_content'][$i] = [
          '#type' => 'fieldset',
        ];
        $form['payload_fieldset']['payload_content'][$i]['key'][$i] = [
          '#type' => 'textfield',
          '#default_value' => $pkeys[$i],
          '#size' => 10,
        ];
        $form['payload_fieldset']['payload_content'][$i]['value'][$i] = [
          '#type' => $type,
          '#placeholder' => $this->t('Value'),
        ];
        if ($type === 'select') {
          $options = [];
          foreach ($pvalues_array[$i][$type] as $o) {
            $o = preg_replace('/\s+/', '', $o);
            $options[$o] = $o;
          }
          $form['payload_fieldset']['payload_content'][$i]['value'][$i]['#options'] =
            $options;
          $form['payload_fieldset']['payload_content'][$i]['value'][$i]['#attributes'] = [
            'style' => 'max-width: 20em',
          ];
        }
        if ($type === 'textfield') {
          $form['payload_fieldset']['payload_content'][$i]['value'][$i]['#type'] = 'entity_autocomplete';
          $form['payload_fieldset']['payload_content'][$i]['value'][$i]['#target_type'] = 'node';
        }
      }
    }
    else {
      for ($i = 0; $i < $payload_field; $i++) {
        $form['payload_fieldset']['payload_content'][$i]['key'][$i] = [
          '#type' => 'textfield',
          '#placeholder' => $this->t('Key'),
          '#size' => 30,
        ];
        $form['payload_fieldset']['payload_content'][$i]['value'][$i] = [
          '#type' => 'textfield',
          '#placeholder' => $this->t('Value'),
          '#size' => 30,
        ];
      }
    }

    $form['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#show_restricted' => TRUE,
      '#token_types' => $relationship_token_types,
    ];
    $form['remember_push_notification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remember Push Notification'),
      '#default_value' => ($remember_push_notification == 1) ? $remember_push_notification : FALSE,
    ];
    return $form;
  }

  /**
   * Submit handler for the action configuration form.
   *
   * If not implemented, the cleaned form values will be
   * passed direclty to the action $configuration parameter.
   *
   * @param array $form
   *   Form array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // This is not required here, when this method is not defined,
    // form values are assigned to the action configuration by default.
    // This function is a must only when user input processing is needed.
    if (!$form_state->getErrors()) {
      $config = \Drupal::service('config.factory')->getEditable('push_notification_vbo.settings');
      $payload = [];

      if ($form_state->getValue('remember_push_notification') == 1) {
        $config->set('remember_push_notification', 1);
        $config->set('push_notification_title', $form_state->getValue('push_notification_title'));
        $config->set('push_notification_body', $form_state->getValue('push_notification_body'));

        $payload_values = $form_state->getValue(['payload_fieldset']);
        $i = 0;
        if (!empty($payload_values['payload_content'])) {
          foreach ($payload_values['payload_content'] as $payload_value) {
            if ($payload_value['key'][$i] != '' && $payload_value['value'][$i] != '') {
              $payload[$payload_value['key'][$i]] = $payload_value['value'][$i];
            }
            $i++;
          }
        }
        $config->set('payload', $payload);
        $config->save();
      }
      else {
        $config->set('remember_push_notification', 0);
        $config->save();
      }

      $this->configuration = [];
      $this->configuration['push_notification_title'] = $form_state->getValue('push_notification_title');
      $this->configuration['push_notification_body'] = $form_state->getValue('push_notification_body');
      $this->configuration['payload'] = $payload;

    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processMessage($current_row, $message) {
    // Get view display's relationship info to build the token data.
    $relationships = $this->view->display_handler->getHandlers('relationship');
    $result_row = $this->view->result[$current_row];

    // Add view display's base entity type into token data.
    $entity_type = $this->view->getBaseEntityType()->id();
    $token_data[$entity_type] = $result_row->_entity;

    // Build token data by relationship.
    foreach ($relationships as $relationship) {
      if (isset($relationship->definition['entity type'])) {
        $entity_type = $relationship->definition['entity type'];
        $relationship_id = $relationship->options['id'];
        $token_data[$entity_type] = $result_row->_relationship_entities[$relationship_id];
      }
    }

    $result = \Drupal::token()->replace($message, $token_data);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {

    /*
     * All config resides in $this->configuration.
     * Passed view rows will be available in $this->context.
     * Data about the view used to select results and optionally
     * the batch context are available in $this->context or externally
     * through the public getContext() method.
     * The entire ViewExecutable object  with selected result
     * rows is available in $this->view or externally through
     * the public getView() method.
     */

    $device_token_field_name = $this->configuration['device_token_field_name'];
    $current_batch_count = $this->context['sandbox']['current_batch'] - 1;
    $device_token = $this->view->style_plugin->getFieldValue($current_batch_count, $device_token_field_name);
    $title = $this->context['configuration']['push_notification_title'];
    $body = $this->context['configuration']['push_notification_body'];
    $payload = $this->context['configuration']['payload'];

    $processed_title = $this->processMessage($current_batch_count, $title);
    $processed_body = $this->processMessage($current_batch_count, $body);

    $this->push_service->setRecipients($device_token);
    $this->push_service->setNotification([
      'title' => $processed_title,
      'body' => $processed_body,
      'badge' => 1,
    ]);

    $this->push_service->setOptions(['priority' => 'normal']);
    if ($payload && is_array($payload) && count($payload) > 0) {
      $this->push_service->setData($payload);
    }
    $this->push_service->send();

    $this->context['sandbox']['current_batch']++;
  }

}
