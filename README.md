VBO Push Notifications
======================

Allows sending customized Push Notifications via Firebase, with all the available flexibility of Views Bulk Operations (VBO) and the Token module.

##Configuration

1. [Install as usual](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)
2. Create a view having the field storing the device registration token
3. Add the field “Global: Views bulk operations”
4. Select the ‘Send Push Notification’ action
5. Choose the field used for storing a device's registration token
6. Configure the view as needed and save it
7. Run the view and choose where to send the Push Notifications

##Dependencies:

- [Views Bulk Operations](https://www.drupal.org/project/views_bulk_operations)
- [Firebase Push Notification (FCM)](https://www.drupal.org/project/firebase)
- [Token](https://www.drupal.org/project/token)


##Supporting organization:
[Bloomidea](https://bloomidea.com/en)

